---
title: "Arrowhead Tools"
date: 2019-05-01T00:00:00-00:00
project_logo: "images/research/arrowheadtools.png"
tags: ["System of Systems", "IoT", "Industry 4.0"]
homepage: "https://tools.arrowhead.eu/home"
facebook: "https://m.facebook.com/vah.sah.14"
linkedin: "https://www.linkedin.com/groups/5071265/"
twitter: "https://twitter.com/arrowheadeu"
youtube: "https://www.youtube.com/user/ArrowheadProject"
funding_bodies: ["horizon2020","ecsel","artemis"]
eclipse_projects: [iot.arrowhead]
project_topic: "I4.0 / CPS"
summary: "Open Source Platform for IoT and System of Systems"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **Arrowhead Tools**  

The Arrowhead Tools project aims for digitalisation and automation solutions for the European industry, which will close the gaps that hinder the IT/OT integration by introducing new technologies in an open source platform for the design and run-time engineering of IoT and System of Systems.


The Arrowhead Tools project is a joint effort of 82 partners from 18 countries, proudly coordinated by Luleå University of Technology. See [here](https://www.arrowhead.eu/arrowheadtools/partners) for the full list of partners.    


The Arrowhead Framework enables the design and implementation of automation systems in application domains such as production, smart cities, e-mobility, energy, and buildings. It was created to efficiently address Industry 4.0 requirements, primarily through scalable, secure, and flexible information sharing that enables system interoperability and integration. To achieve these goals, the SOA architecture abstracts each interface that exchanges information as a service. Instead of hardwiring the connections, it enables loose coupling, late binding, and lookups to discover services.

Since 2016 when the Arrowhead Framework was released, a number of other European Union and national projects have added to it. As a result, the Arrowhead Framework architecture and its reference implementation can be used to implement Industry 4.0 architectures, such as the [Reference Architecture Model for Industry 4.0 (RAMI 4.0)](https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/rami40-an-introduction.html) and the [Industrial Internet Reference Architecture (IIRA)](https://www.iiconsortium.org/IIRA.htm).

The Arrowhead Framework architecture has already been applied in:

- Industrial control systems, such as supervisory control and data acquisition (SCADA) and distributed control systems (DCSs)

- Manufacturing execution systems (MESs)

- Programmable logic controllers (PLCs)

- IoT solutions, such as building energy management, industrial gateways for smart city applications, and intelligent rock bolts for mining safety


Arrowhead Tools was running from May 2019 to June 2022."

---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Eclipse Arrowhead
The [Eclipse Arrowhead](https://projects.eclipse.org/projects/iot.arrowhead) project was created to provide long-term governance and promotion of the Arrowhead Framework, a Service Oriented Architecture (SOA) with a reference implementation for Internet of Things (IoT) interoperability that was originally developed as part of the Arrowhead Tools European research project .

To a large extent, automation is geographically local. Combining local automation with real-time and security requirements leads to the concept of self-contained local clouds. A local cloud is a private network that becomes a shell within which sensitive functionality is grouped. If the private network has a real-time, physical network layer, hard real-time performance can be realized with the local cloud. 

The Eclipse Arrowhead project is based on an SOA that features loose coupling, late binding, and lookups. Together, these features enable discovery of services in operation. They also enable run-time definition of service bindings and provide autonomous service exchange operation until further notice. These capabilities are supported by the Arrowhead core systems ServiceRegistry and Orchestration. In addition, security of service exchanges requires authentication of the service consumer and authorization of the specific service consumption. This is supported by the Arrowhead core system Authorisation.

# Consortium

* LULEA TEKNISKA UNIVERSITET
* AEE - INSTITUT FUR NACHHALTIGE TECHNOLOGIEN
* ACCIONA CONSTRUCCION SA
* ARCELIK A.S.
* AVL LIST GMBH
* BETTERSOLUTIONS SA
* BNEARIT AB
* BOLIDEN MINERAL AB
* dotGIS corporation
* ECLIPSE FOUNDATION EUROPE GMBH
* EQUA SIMULATION AB
* EUROTECH SPA
* EVOLARIS NEXT LEVEL GMBH
* FAGOR ARRASATE S COOP
* FAGOR AUTOMATION S COOP LTDA
* POLITECHNIKA GDANSKA
* HONEYWELL INTERNATIONAL SRO
* IKERLAN S COOP
* INFINEON TECHNOLOGIES AUSTRIA AG
* INFINEON TECHNOLOGIES DRESDEN GMBH& CO KG
* INFINEON TECHNOLOGIES AG
* USTAV TEORIE INFORMACE A AUTOMATIZACE AV CR VVI
* INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
* CONSORZIO NAZIONALE INTERUNIVERSITARIO PER LA NANOELETTRONICA
* JOTNE EPM TECHNOLOGY AS
* KAI KOMPETENZZENTRUM AUTOMOBIL - UND INDUSTRIEELEKTRONIK GMBH
* LINDBACKS BYGG AB
* Lundqvist Trävaru AB
* MONDRAGON GOI ESKOLA POLITEKNIKOA JOSE MARIA ARIZMENDIARRIETA S COOP
* MONDRAGON SISTEMAS DE INFORMACION SOCIEDAD COOPERATIVA
* HOGSKOLEN I OSTFOLD
* PHILIPS MEDICAL SYSTEMS NEDERLAND BV
* PODCOMP AB
* POLITECNICO DI TORINO
* SANTER REPLY SPA
* KNOWLEDGE CENTRIC SOLUTIONS SL
* STMICROELECTRONICS SRL
* SINTEF AS
* TELLU IOT AS
* TECHNISCHE UNIVERSITAET DRESDEN
* TECHNISCHE UNIVERSITAET KAISERSLAUTERN
* UNIVERSIDAD CARLOS III DE MADRID
* ULMA EMBEDDED SOLUTIONS S COOP
* Kompetenzzentrum - Das Virtuelle Fahrzeug, Forschungsgesellschaft mbH
* SIRRIS HET COLLECTIEF CENTRUM VAN DE TECHNOLOGISCHE INDUSTRIE
* 3E NV
* VOLVO LASTVAGNAR AB
* ELEKTRONIKAS UN DATORZINATNU INSTITUTS
* FACHHOCHSCHULE BURGENLAND GMBH
* Masarykova univerzita
* VYSOKE UCENI TECHNICKE V BRNE
* CESKE VYSOKE UCENI TECHNICKE V PRAZE
* ROPARDO SRL
* SAP Norway AS
* AllThingsTalk NV
* INSTITUTE FUR ENGINEERING DESING OF MECHATRONIC SYSTEMS UND MPLM EV
* CISC SEMICONDUCTOR GMBH
* INSTITUT FUER AUTOMATION UND KOMMUNIKATION E.V. MAGDEBURG
* COMMISSARIAT A L ENERGIE ATOMIQUE ET AUX ENERGIES ALTERNATIVES
* MAGILLEM DESIGN SERVICES SAS
* TECHNEXT
* Imagerie Medicale de la Plaine de France
* STMICROELECTRONICS GRENOBLE 2 SAS
* AITIA INTERNATIONAL INFORMATIKAI ZARTKORUEN MUKODO RT
* EVOPRO INFORMATIKAI ES AUTOMATIZALASI KFT
* BUDAPESTI MUSZAKI ES GAZDASAGTUDOMANYI EGYETEM
* INCQUERY LABS KUTATAS-FEJLESZTESI KFT
* VIRTUAL POWER SOLUTIONS SA
* UNIVERSIDADE NOVA DE LISBOA
* TECHNISCHE UNIVERSITAET GRAZ
* UNIVERSITAT ZU LUBECK
* BEIA CONSULT INTERNATIONAL SRL
* TECHNISCHE UNIVERSITEIT EINDHOVEN
* AIT AUSTRIAN INSTITUTE OF TECHNOLOGY GMBH
* SYSTEMA SYSTEMENTWICKLUNG DIPL INF.MANFRED AUSTEN GMBH
* Semantis Information Builders GmbH
* ROBERT BOSCH GMBH
* ASML NETHERLANDS B.V.
* ICT AUTOMATISERING NEDERLAND BV
* EQUA Solutions AG
* Hochschule fuer Technik und Wirtschaft Dresden
* BOSCH SOFTWARE INNOVATIONS GMBH
* ASSYSTEM GERMANY GMBH
* MONDRAGON CORPORACION COOPERATIVA SCOOP
* WAPICE OY
* ABB OY
* Teknologian tutkimuskeskus VTT Oy
* TECHNOLUTION BV

{{</ grid/div>}}
{{</ grid/div>}}
